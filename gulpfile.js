const gulp = require('gulp');
const ts = require('gulp-typescript');
const del = require('del');
const zip = require('gulp-zip');

const clean = () => {
    return del('dist/**', { force: true });
};

const compile = () => {
    return gulp.src('src/**/*.ts')
        .pipe(ts({
            noImplicitAny: false,
        }))
        .pipe(gulp.dest('dist/'));
};

const copy = () => {
    return gulp.src(['public/manifest.json', 'public/iframe.html'])
        .pipe(gulp.dest('dist/'));
}

const release = () => {
    return gulp.src('dist/*')
		.pipe(zip('release.zip'))
		.pipe(gulp.dest('.'))
}

const dev = gulp.series(clean, compile, copy)

exports.default = dev;
exports.clean = clean;
exports.compile = compile;
exports.release = gulp.series(dev, release);
