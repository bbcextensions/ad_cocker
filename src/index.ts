chrome.declarativeNetRequest.onRuleMatchedDebug.addListener((e) => {
    const msg = `Navigation to ${e.request.url} redirected on tab ${e.request.tabId}.`;
    console.log(msg);
});

const netRequest = chrome.declarativeNetRequest

const createRules = () => [
    "net", "com", "org", "live"
].map((tld, mul) => [
    "a.adtng","adnxs-simple", "adnx", "2mdn", "doubleclick", "ads.pexi","doubleclick","pierlinks","hczog","ads.cdn","*.googlesyndication", "*.safeframe.googlesyndication"
].map((domain, id, arr) => ({
    id: (mul * arr.length) + id + 1,
    priority: (mul * arr.length) + id + 1,
    condition: {
        urlFilter: `*://${domain}.${tld}/*`,
        resourceTypes: [netRequest.ResourceType.MAIN_FRAME, netRequest.ResourceType.SUB_FRAME, netRequest.ResourceType.IMAGE],
    },
    action: {
        type: netRequest.RuleActionType.REDIRECT,
        redirect: {
            'url': `chrome-extension:///${chrome.runtime.id}/iframe.html`
        },
    },
}))).flat();

chrome.runtime.onInstalled.addListener(() => {
    const rules = createRules();


    chrome.declarativeNetRequest.updateDynamicRules({
        removeRuleIds: rules.map((_, i) => i + 1),
        addRules: rules,
    });

    console.log('Rules Updated.', createRules());
});
