document.addEventListener("DOMContentLoaded", () => {
    const randomIntFromInterval = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1) + min);
    const interval = randomIntFromInterval(1000, 5000);
    const transition = interval / 10;

    const div = document.querySelector("#img-container");

    const getRandomUrl = () => {
        const { clientHeight, clientWidth } = document.body;
        const imageNr = randomIntFromInterval(1, 111);
        const baseURL = `https://ik.imagekit.io/come4bbsee/BBC/`;
        const edits = `tr:fo-custom,fo-auto,w-${clientWidth*2},h-${clientHeight*2}`;
        return `${baseURL}${edits},tr:oi-blacked.png,oy-N0,ox-N0,ow-200/ad_cocker_${imageNr}.jpg`;
    };

    const createRandomImage = (transition) => {
        const img = document.createElement("img");
        img.src = getRandomUrl();
        img.style.transition = `${Math.floor(transition)}ms ease`;
        return img;
    };

    if(!div) {
        return
    }

    div.appendChild(createRandomImage(transition));
    div.appendChild(createRandomImage(transition));

    const replaceInner = () => {
        const [hiddenImg, shownImg] = Array.from(div.children as HTMLCollectionOf<HTMLImageElement>);

        shownImg.style.opacity = '0';

        setTimeout(() => {
            try{
                div.insertBefore(createRandomImage(transition), hiddenImg);
                div.removeChild(shownImg);
                window.URL.revokeObjectURL(shownImg.src);
            } catch{}
        }, transition);
    };

    setInterval(replaceInner, interval);
    replaceInner();
});
