
## Installation instructions

* Download release [zip file](https://gitlab.com/bbcextensions/ad_cocker/-/releases).
* Unzip in desired location.
* Turn on developer mode in [chrome extensions](chrome://extensions).
* Click `load unpacked` in top left corner.
* Point to unzipped folder.

♠ Enjoy ♠ that BBC 🍆💕

## Todo

* [ ] Add settings for specific sites?
* [ ] Add setting for incognito?
* [ ] ?
